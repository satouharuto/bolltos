﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TimerDirector : MonoBehaviour
{
    public Image Timer;
    public bool roop;
    public float countTime = 5.0f;
    // Update is called once per frame
    void Update()
    {
        if (roop)
        {
            Timer.fillAmount -= 1.0f / countTime * Time.deltaTime;
        }
    }
}
