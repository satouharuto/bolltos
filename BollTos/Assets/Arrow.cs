﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    public GameObject BallPrefab;
    Rigidbody2D rigid2D;
    //float shootForce = 1.0f;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow))
            transform.Rotate(0, 0, 1);
        if(Input.GetKey(KeyCode.LeftArrow))
            transform.Rotate(0, 0, -1);
        if(Input.GetKeyDown(KeyCode.Space))
         BallPrefab = Instantiate(BallPrefab);
       
        BallPrefab.transform.position = transform.position;


        
    }
}
